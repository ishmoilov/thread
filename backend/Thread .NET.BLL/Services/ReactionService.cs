﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class ReactionService : BaseService
    {
        public ReactionService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId  &&  x.IsLike == true);
            
            var dislikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == false );

            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);

                await _context.SaveChangesAsync();

                return;
            }            

            _context.PostReactions.RemoveRange(dislikes);
            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }

        public async Task DislikePost(NewReactionDTO reaction)
        {
            var dislikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId && x.IsLike == false );

            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId  &&  x.IsLike == true);
            
            if (dislikes.Any())
            {
                _context.PostReactions.RemoveRange(dislikes);
                
                await _context.SaveChangesAsync();

                return;
            }

            _context.PostReactions.RemoveRange(likes);
            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var dislikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == false );

            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId  &&  x.IsLike == true);
            
            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);

                await _context.SaveChangesAsync();

                return;
            }            

            _context.CommentReactions.RemoveRange(dislikes);
            _context.CommentReactions.Add(new DAL.Entities.CommentReaction()
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
        
        public async Task DislikeComment(NewReactionDTO reaction)
        {
            var dislikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId && x.IsLike == false );

            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId  &&  x.IsLike == true);
            
            if (dislikes.Any())
            {
                _context.CommentReactions.RemoveRange(dislikes);

                await _context.SaveChangesAsync();

                return;
            }            

            _context.CommentReactions.RemoveRange(likes);
            _context.CommentReactions.Add(new DAL.Entities.CommentReaction()
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    URL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    AvatarId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Images_AvatarId",
                        column: x => x.AvatarId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    PreviewId = table.Column<int>(nullable: true),
                    Body = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Posts_Images_PreviewId",
                        column: x => x.PreviewId,
                        principalTable: "Images",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RefreshTokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Token = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RefreshTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RefreshTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    AuthorId = table.Column<int>(nullable: false),
                    PostId = table.Column<int>(nullable: false),
                    Body = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PostReactions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    IsLike = table.Column<bool>(nullable: true),
                    PostId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostReactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PostReactions_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PostReactions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CommentReactions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    IsLike = table.Column<bool>(nullable: true),
                    CommentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommentReactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CommentReactions_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CommentReactions_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Images",
                columns: new[] { "Id", "CreatedAt", "URL", "UpdatedAt" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 14, 22, 22, 27, 45, DateTimeKind.Local).AddTicks(8249), "https://s3.amazonaws.com/uifaces/faces/twitter/bfrohs/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(3738) },
                    { 23, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8436), "https://picsum.photos/640/480/?image=41", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8439) },
                    { 24, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8451), "https://picsum.photos/640/480/?image=103", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8454) },
                    { 25, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8466), "https://picsum.photos/640/480/?image=60", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8466) },
                    { 26, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8477), "https://picsum.photos/640/480/?image=499", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8480) },
                    { 27, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8492), "https://picsum.photos/640/480/?image=1046", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8495) },
                    { 28, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8507), "https://picsum.photos/640/480/?image=589", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8507) },
                    { 29, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8556), "https://picsum.photos/640/480/?image=657", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8556) },
                    { 30, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8571), "https://picsum.photos/640/480/?image=1060", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8571) },
                    { 31, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8586), "https://picsum.photos/640/480/?image=5", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8586) },
                    { 32, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8597), "https://picsum.photos/640/480/?image=934", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8600) },
                    { 33, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8612), "https://picsum.photos/640/480/?image=6", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8615) },
                    { 34, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8627), "https://picsum.photos/640/480/?image=116", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8627) },
                    { 35, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8638), "https://picsum.photos/640/480/?image=736", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8641) },
                    { 36, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8653), "https://picsum.photos/640/480/?image=613", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8653) },
                    { 37, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8665), "https://picsum.photos/640/480/?image=924", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8668) },
                    { 38, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8679), "https://picsum.photos/640/480/?image=839", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8682) },
                    { 39, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8694), "https://picsum.photos/640/480/?image=55", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8694) },
                    { 22, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8413), "https://picsum.photos/640/480/?image=886", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8419) },
                    { 40, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8706), "https://picsum.photos/640/480/?image=547", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8709) },
                    { 21, new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(7775), "https://picsum.photos/640/480/?image=202", new DateTime(2019, 6, 14, 22, 22, 27, 50, DateTimeKind.Local).AddTicks(8217) },
                    { 19, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(5006), "https://s3.amazonaws.com/uifaces/faces/twitter/desastrozo/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(5009) },
                    { 2, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4693), "https://s3.amazonaws.com/uifaces/faces/twitter/alexandermayes/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4699) },
                    { 3, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4722), "https://s3.amazonaws.com/uifaces/faces/twitter/spedwig/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4725) },
                    { 4, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4740), "https://s3.amazonaws.com/uifaces/faces/twitter/maz/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4740) },
                    { 5, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4754), "https://s3.amazonaws.com/uifaces/faces/twitter/prheemo/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4754) },
                    { 6, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4769), "https://s3.amazonaws.com/uifaces/faces/twitter/arkokoley/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4769) },
                    { 7, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4781), "https://s3.amazonaws.com/uifaces/faces/twitter/increase/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4783) },
                    { 8, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4795), "https://s3.amazonaws.com/uifaces/faces/twitter/zforrester/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4798) },
                    { 9, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4810), "https://s3.amazonaws.com/uifaces/faces/twitter/_scottburgess/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4813) },
                    { 10, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4824), "https://s3.amazonaws.com/uifaces/faces/twitter/linkibol/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4827) },
                    { 11, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4839), "https://s3.amazonaws.com/uifaces/faces/twitter/heykenneth/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4839) },
                    { 12, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4851), "https://s3.amazonaws.com/uifaces/faces/twitter/slowspock/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4909) },
                    { 13, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4924), "https://s3.amazonaws.com/uifaces/faces/twitter/y2graphic/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4927) },
                    { 14, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4936), "https://s3.amazonaws.com/uifaces/faces/twitter/illyzoren/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4939) },
                    { 15, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4950), "https://s3.amazonaws.com/uifaces/faces/twitter/kianoshp/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4953) },
                    { 16, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4965), "https://s3.amazonaws.com/uifaces/faces/twitter/nasirwd/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4965) },
                    { 17, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4977), "https://s3.amazonaws.com/uifaces/faces/twitter/angelcreative/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4980) },
                    { 18, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4991), "https://s3.amazonaws.com/uifaces/faces/twitter/overra/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(4994) },
                    { 20, new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(5018), "https://s3.amazonaws.com/uifaces/faces/twitter/angelcreative/128.jpg", new DateTime(2019, 6, 14, 22, 22, 27, 46, DateTimeKind.Local).AddTicks(5021) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 21, null, new DateTime(2019, 6, 14, 22, 22, 27, 216, DateTimeKind.Local).AddTicks(5498), "test@gmail.com", "K8qXvrhjbfQM6I+sghiKaw8lpSge/e8Czz8UMq1k4os=", "YtOLBIHnoVSleMdD7DpFNtWrCb02HYIsGSRb8+jMuZM=", new DateTime(2019, 6, 14, 22, 22, 27, 216, DateTimeKind.Local).AddTicks(5498), "testUser" });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[,]
                {
                    { 20, 21, "suscipit", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1600), 26, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1603) },
                    { 15, 21, @"Molestiae facere aut veritatis aut eum.
                Nemo molestias earum explicabo recusandae quo voluptatem.
                Est ut consectetur incidunt.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1208), 25, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1208) },
                    { 2, 21, "Numquam iure sit sequi. Et quisquam maxime aut ut. Quia ipsa dolor repellat nulla et rem velit. Odit dolor necessitatibus necessitatibus.", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(7549), 35, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(7558) },
                    { 16, 21, "Et quas sed voluptatibus officiis voluptates nostrum. Rerum vitae id omnis enim id est eaque ducimus illo. Illo eaque fugiat corporis.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1290), 38, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1290) }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[,]
                {
                    { 9, 19, new DateTime(2019, 6, 14, 22, 22, 27, 134, DateTimeKind.Local).AddTicks(8079), "Kaylee.Cole@hotmail.com", "SmyY8N3EPalpAV2sVvxFwOuJEqEsLDGrAJjc3nsBbiM=", "WdK3CIPiN5aE8VqHtvz4QKOGfTND7Ffer5M2YPK3v5Y=", new DateTime(2019, 6, 14, 22, 22, 27, 134, DateTimeKind.Local).AddTicks(8085), "Jermaine60" },
                    { 3, 19, new DateTime(2019, 6, 14, 22, 22, 27, 93, DateTimeKind.Local).AddTicks(8927), "Bruce64@yahoo.com", "DhfejDizPYRmSsSuNmOuuJeq+QsttP3aldke6nJbdNc=", "EKCdR+RwCHndEx3rAiR615xUBFuw5cTtxRISM9tSSo0=", new DateTime(2019, 6, 14, 22, 22, 27, 93, DateTimeKind.Local).AddTicks(8941), "Jarrell_Lehner38" },
                    { 20, 16, new DateTime(2019, 6, 14, 22, 22, 27, 209, DateTimeKind.Local).AddTicks(7820), "Jan23@hotmail.com", "+ZxM9sdAhvYfmd66yAr0LWk6B5OhzMqMCpCNb8EAfEo=", "iZxpSHlY0UOzWHgfeL50iVA/HiuGOlNXR02ewRmQfLk=", new DateTime(2019, 6, 14, 22, 22, 27, 209, DateTimeKind.Local).AddTicks(7831), "Vern35" },
                    { 15, 15, new DateTime(2019, 6, 14, 22, 22, 27, 175, DateTimeKind.Local).AddTicks(5237), "Gage_Lehner13@gmail.com", "MIoHS9liwASRv7KGXFnYLK1jSjLjGdrqF1zc8eMCPtM=", "rZGmk95W/5OcrG9EPbml0wWvRUG7DFx8vUZZiIGN8qk=", new DateTime(2019, 6, 14, 22, 22, 27, 175, DateTimeKind.Local).AddTicks(5246), "Terrence.Reichert" },
                    { 14, 15, new DateTime(2019, 6, 14, 22, 22, 27, 168, DateTimeKind.Local).AddTicks(5849), "Bertram.Christiansen@gmail.com", "a2CEb0p8vz9y487ky292xWCwUlN4POdzeUEvBkbkQHo=", "DXszTJqY8dC+ocvPNJEBEBoZplykPCRB1OgH6TccI4c=", new DateTime(2019, 6, 14, 22, 22, 27, 168, DateTimeKind.Local).AddTicks(5857), "Leslie53" },
                    { 17, 14, new DateTime(2019, 6, 14, 22, 22, 27, 189, DateTimeKind.Local).AddTicks(1546), "Elmer_Dach62@hotmail.com", "sGFr9lV7b+GN2DsnpW5Gidug2mSt/ckMU0fPZiZstX8=", "KDYHRjQoLh+13RBmvVUb9kZn2pIyCGHZSZMbATgM7M0=", new DateTime(2019, 6, 14, 22, 22, 27, 189, DateTimeKind.Local).AddTicks(1560), "Sharon.Dietrich78" },
                    { 8, 14, new DateTime(2019, 6, 14, 22, 22, 27, 128, DateTimeKind.Local).AddTicks(640), "Napoleon_Schamberger@hotmail.com", "WZYoO9CCACxQ4Ax3qqkffD/2VB+znRX5PmRtooM57jQ=", "M0CfyCLrTn3jBptMApje9aH1ED0HyOv9CwWtLYuYV/g=", new DateTime(2019, 6, 14, 22, 22, 27, 128, DateTimeKind.Local).AddTicks(652), "Suzanne_Homenick73" },
                    { 6, 13, new DateTime(2019, 6, 14, 22, 22, 27, 114, DateTimeKind.Local).AddTicks(4109), "Marjolaine.Crona@hotmail.com", "beZmciD3ZiQZM0CMxAXz902dusaxCnuUGgWxZMy7b30=", "whxSZGYs+bDx/34zqUp3wuduKfzhqq+44I5KRI+8UAU=", new DateTime(2019, 6, 14, 22, 22, 27, 114, DateTimeKind.Local).AddTicks(4123), "Carlee.Denesik93" },
                    { 18, 11, new DateTime(2019, 6, 14, 22, 22, 27, 196, DateTimeKind.Local).AddTicks(609), "Delbert_Wolff65@gmail.com", "aJXL9eMxY6upqNOsgZrJ7JqnxXtNdsRMPevQ23cWRrc=", "EOaSQAM8hPzmcx3cp4REQ7ng5xxgBpddA7XtXyOIAjc=", new DateTime(2019, 6, 14, 22, 22, 27, 196, DateTimeKind.Local).AddTicks(624), "Wyman79" },
                    { 4, 10, new DateTime(2019, 6, 14, 22, 22, 27, 100, DateTimeKind.Local).AddTicks(7686), "Abigail12@hotmail.com", "Oi2+2OlwU24OocFATXFckxEi6ecQzFwaDGnOBaVaBik=", "o1EnR8kN5K0Ze22BMREJGsO87OlpQpaFDvps/qsEUCc=", new DateTime(2019, 6, 14, 22, 22, 27, 100, DateTimeKind.Local).AddTicks(7697), "Tyreek.Jacobs" },
                    { 19, 8, new DateTime(2019, 6, 14, 22, 22, 27, 202, DateTimeKind.Local).AddTicks(9905), "Beulah63@gmail.com", "XDrIlQZe2JAhF2giFj9BUA9L+czIpACkjKOoIfOfBIw=", "5ebZVGuR4tFHhe4xFMLwK01kPwSASizjIXQyC6PnOwo=", new DateTime(2019, 6, 14, 22, 22, 27, 202, DateTimeKind.Local).AddTicks(9919), "Arvid_Jenkins" },
                    { 16, 8, new DateTime(2019, 6, 14, 22, 22, 27, 182, DateTimeKind.Local).AddTicks(3145), "Maia_Heidenreich56@hotmail.com", "AZjRfTOH7nyIxZSZwxDc6ZCt9wPmdKrlI5ZPSnHSAsY=", "5x25doxY/bQhwNCH/Pwco6j7mSzUoSdz4mXljio6pt8=", new DateTime(2019, 6, 14, 22, 22, 27, 182, DateTimeKind.Local).AddTicks(3154), "Cary.Halvorson38" },
                    { 11, 7, new DateTime(2019, 6, 14, 22, 22, 27, 148, DateTimeKind.Local).AddTicks(2555), "Lori.Kohler@gmail.com", "ZmxxS+aTk8zt5N4Xq0UZDjuPG/CBRhAMOkx3LMKxBw0=", "Szyue/h+mHPquWoHhLxqoACV3cyLbi1sOgttPDiReZY=", new DateTime(2019, 6, 14, 22, 22, 27, 148, DateTimeKind.Local).AddTicks(2561), "Clyde.Tillman" },
                    { 1, 7, new DateTime(2019, 6, 14, 22, 22, 27, 80, DateTimeKind.Local).AddTicks(2503), "Irma72@yahoo.com", "UUMIDPwcb3+924p4Rdxc/OlBvrmTK7+uFbsGiqCUf30=", "AC4DOalqmg2KIzJ5XNmmaR8yLIlbYq3wNChJJYGwUr4=", new DateTime(2019, 6, 14, 22, 22, 27, 80, DateTimeKind.Local).AddTicks(2980), "Amari_Schaden" },
                    { 5, 6, new DateTime(2019, 6, 14, 22, 22, 27, 107, DateTimeKind.Local).AddTicks(4922), "Alene_Bashirian99@hotmail.com", "H7ZRbuQQkN4OG0DAn7BdAws0Grw0BdoYCJP+Rc29pz8=", "YfW2tDq7672ak+AE2c5Dp0n6cRz5l0eB7TmFluVsHCs=", new DateTime(2019, 6, 14, 22, 22, 27, 107, DateTimeKind.Local).AddTicks(4925), "Kaden19" },
                    { 12, 5, new DateTime(2019, 6, 14, 22, 22, 27, 155, DateTimeKind.Local).AddTicks(94), "Madalyn_Gottlieb11@yahoo.com", "YTqQbPjAsrSAjD3RmNWcB0Su5deyl1VzBsJ7nyzgS50=", "9KR+ee2c2pnTJMYr0AyP7UaML9yfHcJWz4HcsRDm3FI=", new DateTime(2019, 6, 14, 22, 22, 27, 155, DateTimeKind.Local).AddTicks(105), "Burley_Nader" },
                    { 7, 4, new DateTime(2019, 6, 14, 22, 22, 27, 121, DateTimeKind.Local).AddTicks(2572), "Anissa_Bartell31@hotmail.com", "NwImRMGdhUZwnnUubAiBHt//Gno8jgrPXdkOTfbpCRk=", "WWHgPsfrG09jWywVaOE0LwwFdGlwiLx6tAuLaAMQh7U=", new DateTime(2019, 6, 14, 22, 22, 27, 121, DateTimeKind.Local).AddTicks(2587), "Domenic_Skiles4" },
                    { 13, 1, new DateTime(2019, 6, 14, 22, 22, 27, 161, DateTimeKind.Local).AddTicks(8656), "Alyson_Smitham@hotmail.com", "rnvmBebhI1nkjw31PRdWHrVEJFnF4sMxv0ZiXepdGL4=", "fWDMaECnNJT4735Xz/GY22rAdkjMHhISh+mQLU6ah7c=", new DateTime(2019, 6, 14, 22, 22, 27, 161, DateTimeKind.Local).AddTicks(8668), "Maximilian.Pouros" },
                    { 10, 11, new DateTime(2019, 6, 14, 22, 22, 27, 141, DateTimeKind.Local).AddTicks(6062), "Denis43@hotmail.com", "9zxIfNpQTVXs5bUAm7WN7aRcdPe6+kdF/NPYLvQK97U=", "9DVjUX1IV6vRyEy56MF3F0tW7uT0xpu0+Ws64D7SVUU=", new DateTime(2019, 6, 14, 22, 22, 27, 141, DateTimeKind.Local).AddTicks(6067), "Caroline5" },
                    { 2, 1, new DateTime(2019, 6, 14, 22, 22, 27, 87, DateTimeKind.Local).AddTicks(495), "Kristin30@hotmail.com", "rWdu4e4QOr9yDFNgw3k8KRk8tLRk8t3AQaha0je8Jxc=", "vWh3QgRDQpr2l/qmaDIitG4V+hP/39/soBwoFoRtB/Q=", new DateTime(2019, 6, 14, 22, 22, 27, 87, DateTimeKind.Local).AddTicks(507), "Phoebe.Metz" }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[,]
                {
                    { 20, 12, "Autem voluptas dignissimos iste consequuntur suscipit.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(546), 15, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(549) },
                    { 2, 10, "Numquam enim quis et ullam magni quo voluptatem quis corporis.", new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9758), 2, new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9767) },
                    { 18, 3, "Aut voluptas est vero voluptatem aliquid ex maiores vitae.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(473), 20, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(476) }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 5, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1176), false, 20, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1178), 5 },
                    { 1, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(195), true, 16, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(590), 18 },
                    { 20, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1454), true, 15, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1454), 1 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[,]
                {
                    { 11, 9, @"Reprehenderit aut veritatis sint qui.
                Cum quia eveniet dolor qui illo.
                Et ut consequuntur.
                Dignissimos occaecati cumque omnis magni amet eveniet sit consequatur ratione.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(880), 40, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(886) },
                    { 5, 9, "Et architecto impedit est consequatur. Sequi adipisci illo exercitationem quasi cum. Velit assumenda numquam consequatur tempora. Saepe omnis reprehenderit at aperiam est. Aut doloribus provident reprehenderit. Temporibus ex enim odit dolores ea ducimus.", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9232), 38, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9232) },
                    { 13, 3, "Nihil quaerat dolorum neque minus quisquam magnam eligendi est. Reprehenderit et possimus ab. Est aspernatur numquam ea natus perferendis eligendi. Est suscipit quia esse. Facere harum incidunt omnis dolorem provident quia libero. Et reiciendis commodi quo asperiores magni assumenda.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1114), 23, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1114) },
                    { 3, 15, "Blanditiis consectetur aut.", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(8922), 28, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(8928) },
                    { 1, 15, "Sed et quos voluptatem dolorem culpa voluptas repellendus quo consequatur. Vel laudantium corporis omnis sed nihil ut minima accusantium est. Et labore eos id quas id explicabo alias recusandae nam. Architecto quia in incidunt dolorum magnam provident. Et incidunt quia adipisci nobis est distinctio soluta fuga aut. Iste illum amet incidunt distinctio magnam nostrum eius beatae.", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(6360), 35, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(6779) },
                    { 18, 8, @"Et nulla unde nisi omnis in.
                Nobis labore voluptatem sed recusandae est non eaque.
                Nostrum error quis et fuga.
                Explicabo voluptatem consequatur voluptate omnis voluptas dignissimos.
                Rerum ipsam eaque natus et facere et doloribus et.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1548), 34, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1551) },
                    { 6, 18, "dignissimos", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9674), 25, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9680) },
                    { 14, 10, "ut", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1135), 37, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1135) },
                    { 19, 19, "Quidem quasi tempora enim recusandae.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1580), 39, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1583) },
                    { 17, 19, "Iusto ab voluptatum culpa id. Harum totam fuga unde et mollitia. Magni doloribus possimus cupiditate ducimus esse ullam qui. Temporibus earum facilis delectus. Mollitia velit suscipit voluptatibus veritatis voluptatibus voluptatem. Eveniet id nesciunt ipsa eos ut eos.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1428), 29, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(1431) },
                    { 9, 19, "aperiam", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(751), 34, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(751) },
                    { 12, 11, @"Ullam doloremque non velit.
                Quo vitae sapiente.
                Vero recusandae sed error reiciendis deserunt ea incidunt.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(962), 36, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(965) },
                    { 7, 5, "Reiciendis eius natus aliquam aut veritatis provident sapiente quas.", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9762), 21, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9765) },
                    { 8, 7, @"Quis neque distinctio distinctio.
                Veniam voluptatem consequatur sunt harum vel ea commodi voluptatibus.", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(693), 37, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(699) },
                    { 10, 6, "necessitatibus", new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(775), 29, new DateTime(2019, 6, 14, 22, 22, 27, 233, DateTimeKind.Local).AddTicks(778) },
                    { 4, 7, "Voluptatum non voluptatem alias. Vel impedit numquam. Ut non fugit modi incidunt et eius. Ullam fugit iusto dolores exercitationem esse voluptas rem.", new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9077), 34, new DateTime(2019, 6, 14, 22, 22, 27, 232, DateTimeKind.Local).AddTicks(9080) }
                });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[] { 14, 20, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7634), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7637), 15 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[,]
                {
                    { 16, 16, "Odit similique ipsam quasi placeat velit.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(394), 11, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(397) },
                    { 13, 7, "Dolorum ea ut sed ea rerum excepturi.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(285), 11, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(285) },
                    { 1, 8, "Nemo facere qui reiciendis ratione pariatur corporis labore qui.", new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(8731), 5, new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9144) },
                    { 17, 21, "Praesentium vel iure esse enim.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(429), 18, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(429) },
                    { 19, 17, "Delectus rem et.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(502), 10, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(505) },
                    { 12, 15, "Qui qui iusto ab iusto.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(244), 10, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(244) },
                    { 8, 1, "Aut tempora et nam temporibus dolorem sequi nulla iusto.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(92), 6, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(95) },
                    { 4, 21, "Ab omnis nemo.", new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9870), 6, new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9870) },
                    { 10, 7, "Consequatur vitae repellat placeat.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(174), 17, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(174) },
                    { 6, 6, "At sit illo dolorum est.", new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9972), 17, new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9975) },
                    { 5, 3, "Quis vel ab.", new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9928), 17, new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9931) },
                    { 9, 10, "Veniam nulla ut numquam amet.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(136), 4, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(136) },
                    { 11, 20, "Cupiditate quia veniam corporis nihil.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(209), 4, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(209) },
                    { 15, 15, "Earum deleniti et occaecati voluptatem.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(359), 12, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(359) },
                    { 3, 14, "Eos dolorum qui sunt.", new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9831), 12, new DateTime(2019, 6, 14, 22, 22, 27, 237, DateTimeKind.Local).AddTicks(9834) },
                    { 14, 5, "Dicta doloremque molestiae.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(320), 4, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(320) },
                    { 7, 7, "Ut est dolores alias.", new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(13), 7, new DateTime(2019, 6, 14, 22, 22, 27, 238, DateTimeKind.Local).AddTicks(16) }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1269), false, 9, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1269), 8 },
                    { 3, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1132), false, 4, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1135), 15 },
                    { 8, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1231), false, 13, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1234), 10 },
                    { 18, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1418), false, 3, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1418), 12 },
                    { 17, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1389), true, 18, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1392), 8 },
                    { 9, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1249), false, 4, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1252), 19 },
                    { 13, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1319), true, 10, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1322), 4 },
                    { 7, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1214), false, 8, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1216), 10 },
                    { 16, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1372), false, 12, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1372), 10 },
                    { 19, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1433), true, 8, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1436), 11 },
                    { 14, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1339), true, 6, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1339), 7 },
                    { 11, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1287), false, 6, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1287), 6 },
                    { 6, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1196), true, 7, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1199), 14 },
                    { 12, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1304), true, 11, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1304), 19 },
                    { 4, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1155), false, 12, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1155), 21 },
                    { 2, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1099), false, 10, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1105), 14 },
                    { 15, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1354), true, 17, new DateTime(2019, 6, 14, 22, 22, 27, 244, DateTimeKind.Local).AddTicks(1357), 21 }
                });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 7, 9, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7493), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7493), 21 },
                    { 18, 1, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7710), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7710), 8 },
                    { 8, 1, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7525), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7528), 4 },
                    { 16, 4, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7666), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7669), 21 },
                    { 20, 10, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7745), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7745), 5 },
                    { 15, 10, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7651), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7651), 20 },
                    { 6, 10, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7476), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7479), 15 },
                    { 9, 15, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7546), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7549), 18 },
                    { 19, 1, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7727), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7727), 15 },
                    { 12, 3, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7599), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7602), 4 },
                    { 5, 3, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7458), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7458), 8 },
                    { 3, 7, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7420), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7420), 11 },
                    { 1, 7, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(6486), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(6884), 5 },
                    { 13, 14, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7616), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7619), 12 },
                    { 2, 14, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7388), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7394), 7 },
                    { 4, 11, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7438), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7441), 19 },
                    { 10, 9, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7564), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7566), 2 },
                    { 11, 3, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7581), true, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7584), 2 },
                    { 17, 16, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7684), false, new DateTime(2019, 6, 14, 22, 22, 27, 249, DateTimeKind.Local).AddTicks(7686), 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommentReactions_CommentId",
                table: "CommentReactions",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_CommentReactions_UserId",
                table: "CommentReactions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostReactions_PostId",
                table: "PostReactions",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_PostReactions_UserId",
                table: "PostReactions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_AuthorId",
                table: "Posts",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_PreviewId",
                table: "Posts",
                column: "PreviewId");

            migrationBuilder.CreateIndex(
                name: "IX_RefreshTokens_UserId",
                table: "RefreshTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_AvatarId",
                table: "Users",
                column: "AvatarId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommentReactions");

            migrationBuilder.DropTable(
                name: "PostReactions");

            migrationBuilder.DropTable(
                name: "RefreshTokens");

            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Images");
        }
    }
}

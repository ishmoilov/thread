using System.Threading;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Thread_.NET.DAL.Context
{
    public class ThreadContextFactory : IDesignTimeDbContextFactory<ThreadContext>
    {
        public ThreadContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ThreadContext>();
            optionsBuilder.UseSqlServer("Server=(local);Database=ThreadDB;Trusted_Connection=True;");

            return new ThreadContext(optionsBuilder.Options);
        }
        
    }
}
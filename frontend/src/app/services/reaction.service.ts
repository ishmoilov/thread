import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class ReactionService {
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) { }

    public reactToPost(post: Post, currentUser: User, reaction: boolean) {
        const innerPost = post;

        const entityReaction: NewReaction = {
            entityId: innerPost.id,
            isLike: reaction,
            userId: currentUser.id
        }

        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === reaction);

        if (hasReaction) {

            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id && x.isLike !== null);
        }
        else {
            innerPost.reactions.forEach((x) => {
                if (x.user.id === currentUser.id && x.isLike === !reaction) {
                    x.isLike = null;
                }
            });
            innerPost.reactions = innerPost.reactions.concat({ isLike: reaction, user: currentUser });
        }

        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id && x.isLike === reaction);

        return this.postService.reactToPost(entityReaction, reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                if (hasReaction) {

                    innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id && x.isLike !== null);
                }
                else {
                    innerPost.reactions.forEach((x) => {
                        if (x.user.id === currentUser.id && x.isLike === !reaction) {
                            x.isLike = null;
                        }
                    });
                    innerPost.reactions = innerPost.reactions.concat({ isLike: reaction, user: currentUser });
                }

                return of(innerPost);
            })
        );
    }

    public reactToComment(comment: Comment, currentUser: User, reaction: boolean) {
        const innerComment = comment;

        const entityReaction: NewReaction = {
            entityId: innerComment.id,
            isLike: reaction,
            userId: currentUser.id
        }

        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === reaction);

        if (hasReaction) {

            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id && x.isLike !== null);
        }
        else {
            innerComment.reactions.forEach((x) => {
                if (x.user.id === currentUser.id && x.isLike === !reaction) {
                    x.isLike = null;
                }
            });
            innerComment.reactions = innerComment.reactions.concat({ isLike: reaction, user: currentUser });
        }

        hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id && x.isLike === reaction);

        return this.commentService.reactToComment(entityReaction, reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                if (hasReaction) {

                    innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id && x.isLike !== null);
                }
                else {
                    innerComment.reactions.forEach((x) => {
                        if (x.user.id === currentUser.id && x.isLike === !reaction) {
                            x.isLike = null;
                        }
                    });
                    innerComment.reactions = innerComment.reactions.concat({ isLike: reaction, user: currentUser });
                }

                return of(innerComment);
            })
        );
    }


}

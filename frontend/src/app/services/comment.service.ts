import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { NewReaction } from '../models/reactions/newReaction';
import { Comment } from '../models/comment/comment';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(comment: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public editComment(comment: Comment) {
        return this.httpService.putFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public reactToComment(reaction: NewReaction, isLike: boolean) {
        if (isLike === true) {
            return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/like`, reaction);
        }
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/dislike`, reaction);
    }

    public deleteComment(id: number) {
        return this.httpService.deleteFullRequest<Comment>(`${this.routePrefix}/${id}`);
    }


}

import { Component, Input, OnDestroy } from '@angular/core';
import { Comment } from 'src/app/models/comment/comment';
import { User } from 'src/app/models/user';
import { AuthenticationService } from 'src/app/services/auth.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { AuthDialogService } from 'src/app/services/auth-dialog.service'
import { empty, Observable, Subject } from 'rxjs';
import { ReactionService } from 'src/app/services/reaction.service';
import { CommentService } from 'src/app/services/comment.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { take, catchError, switchMap, takeUntil, map } from 'rxjs/operators';


@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnDestroy {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    //@Input() public post: Post;

    public showCommentEditingContainer = false;
    private unsubscribe$ = new Subject<void>();


    public constructor(
        private reactionService: ReactionService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,

    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public editComment(newCommentBody: string) {
        this.comment.body = newCommentBody;
        this.commentService.editComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => this.comment = response.body),

            (error) => this.snackBarService.showErrorMessage(error);

        this.toggleCommentEditingWindow()
    }

    public deleteComment() {
        this.commentService.deleteComment(this.comment.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((data) => (this.comment = data.body));
    }

    public likeComment() {

    }

    public reactToComment(reaction: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.reactionService.reactToComment(this.comment, userResp, reaction)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.reactionService
            .reactToComment(this.comment, this.currentUser, reaction)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public toggleCommentEditingWindow() {
        this.showCommentEditingContainer = !this.showCommentEditingContainer;
    }

    public isCommentAuthor() {
        if (typeof this.currentUser !== 'undefined' && this.comment) {
            return this.currentUser.id === this.comment.author.id;
        }
    }

    public getReactedUsers(reaction: boolean) {
        var users: string[] = new Array();
        let userReactions = this.comment.reactions.filter((x) => x.isLike === reaction);
        userReactions.forEach((x) => users.push(" " + `${x.user.userName}`));
        return users;
    }

    public getReactions(reaction: boolean) {
        return this.comment.reactions.filter((x) => x.isLike === reaction).length;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}

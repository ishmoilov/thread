import { Component, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Post } from 'src/app/models/post/post';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { ReactionService } from 'src/app/services/reaction.service';
import { NewComment } from 'src/app/models/comment/new-comment';
import { CommentService } from 'src/app/services/comment.service';
import { User } from 'src/app/models/user';
import { Comment } from 'src/app/models/comment/comment';
import { catchError, switchMap, takeUntil, map } from 'rxjs/operators';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { ImgurService } from 'src/app/services/imgur.service';


@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public showEditingContainer = false;
    public newComment = {} as NewComment;
    private unsubscribe$ = new Subject<void>();
    public newImageFile: File;
    public changedImageUrl: string;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private reactionService: ReactionService,
        private postService: PostService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private imgurService: ImgurService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }


    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public reactToPost(reaction: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.reactionService.reactToPost(this.post, userResp, reaction)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.reactionService
            .reactToPost(this.post, this.currentUser, reaction)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public togglePostEditingWindow() {
        this.showEditingContainer = !this.showEditingContainer;
    }


    public editPost(newBody: string) {
        this.post.body = newBody;
        const editPostSubscription = !this.newImageFile
            ? this.postService.editPost(this.post)
            : this.imgurService.uploadToImgur(this.newImageFile, 'editedImage').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.editPost(this.post);
                })
            );

        editPostSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (response) => {
                this.post = response.body
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );

        this.togglePostEditingWindow();
    }

    public deletePost() {
        this.postService.deletePost(this.post.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((data) => (this.post = data.body));
    }

    public changeImage(target: any) {
        this.newImageFile = target.files[0];

        if (!this.newImageFile) {
            target.value = '';
            return;
        }

        if (this.newImageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
        reader.readAsDataURL(this.newImageFile);
    }

    public removeImage() {
        this.post.previewImage = undefined;
        this.newImageFile = undefined;
    }

    public isAuthor() {
        if (typeof this.currentUser !== 'undefined' && this.post) {

            return this.currentUser.id == this.post.author.id;
        }
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public getReactions(reaction: boolean) {
        return this.post.reactions.filter((x) => x.isLike === reaction).length;
    }

    public getReactedUsers(reaction: boolean) {
        var users: string[] = new Array();
        let dislikes = this.post.reactions.filter((x) => x.isLike === reaction);
        dislikes.forEach((x) => users.push(" " + `${x.user.userName}`));
        return users;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

}
